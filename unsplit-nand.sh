#!/bin/sh

# Reset normal style
NORM="\033[0m"

# Font styles
BOLD="\033[1m"
DIM="\033[2m"
ULIN="\033[4m" # Underlined
INVR="\033[7m" # Inverted
HIDN="\033[8m" # Hidden
# Reset font styles
N_BOLD="\033[21m"
N_DIM="\033[22m"
N_ULIN="\033[24m" # Not underlined

# Font colours
DFT="\033[39m" # Default foreground colour
BLK="\033[30m" # Black
RED="\033[31m"
GRN="\033[32m" # Green
YLW="\033[33m" # Yellow
BLU="\033[34m" # Blue
MAG="\033[35m" # Magenta
CYN="\033[36m" # Cyan
GRY="\033[90m" # Grey
WIT="\033[97m" # White
# Light font colours
L_GRY="\033[37m" # Light grey
L_RED="\033[91m" # Light red
L_GRN="\033[92m" # Light green
L_YLW="\033[93m" # Light yellow
L_BLU="\033[94m" # Light blue
L_MAG="\033[95m" # Light magenta
L_CYN="\033[96m" # Light cyan

STARTING_DIR=${PWD}

change_directory() {
    cd "$1"
    if [[ $? -ne 0 ]]; then
        error_report "Couldn't change directory to '$1'" 2
    fi
}

error_report() {
    MSG=$1
    CODE=${2-1} # Code 1 if unspecified
    
    echo -e "\n\n${BOLD}${RED}ERROR: \t${MSG}"
    echo -e "\t${L_RED}Aborting!${NORM}\n"
    
    exit $CODE
}

greatest_common_factor() {
    if [[ $2 -eq 0 ]]; then
        echo $1
    else
        n=$(echo "$1 % $2" | bc)
        greatest_common_factor $2 $n
    fi
}

full_nand() {
    BIN_PARTS="$(ls rawnand.bin.*)"
    PART_COUNT=$(echo "$BIN_PARTS" | wc -l)
    echo -e "Combining $PART_COUNT files."
    echo -e "This will take a while...\n"
    cat $BIN_PARTS > rawnand.bin
    echo -e "\nDone!"
}

user_nand() {
    BIN_PARTS="$(ls USER.*)"
    PART_COUNT=$(echo "$BIN_PARTS" | wc -l)
    echo -e "This will take a while...\n"
    cat $BIN_PARTS > USER.bin
    echo -e "\nDone!"
}

RUN_FUL=false
RUN_USR=false
DIR="${PWD}"

while [[ $# -gt 0 ]]; do
    case $1 in
        -f | --full)
            run_ful=true
        ;;
        -u | --user)
            run_usr=true
        ;;
    esac
    if [[ -d $1 ]]; then
        DIR=$1
    fi
    shift
done

if [[ $DIR == "" ]]; then
    error_report "You need to specify a path!" 1
fi

if [[ $RUN_FUL ]]; then
    echo "changing directory to '${DIR}'"
    change_directory "${DIR}"
    full_nand
fi

if [[ $RUN_USR ]]; then
    echo "changing directory to '${DIR}'"
    change_directory "${DIR}/partitions"
    user_nand
fi

change_directory "$STARTING_DIR"
